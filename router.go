package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"log/slog"
	"net/http"
	"slices"
	"strconv"
	"strings"
	"time"

	osmsun "codeberg.org/Gusted/osm-sun"
	"codeberg.org/Gusted/time/osm"
	"codeberg.org/Gusted/time/tz"
	"github.com/hako/durafmt"
)

// Add middleware that logs incoming requests.
func logHandler(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		logger.Debug("Incoming request", slog.Group("request", "method", r.Method, "url", r.URL))
		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// Construct HTTP routes.
func routes() http.Handler {
	m := http.NewServeMux()

	m.HandleFunc("/{timezone...}", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache")

		locName := r.PathValue("timezone")
		if locName == "" {
			renderTemplate(w, r, "homepage", map[string]any{})
			return
		}

		loc, normalizedName, err := tz.LoadLocation(locName)
		// If there's an error, render the index apge.
		if err != nil {
			renderTemplate(w, r, "homepage", map[string]any{})
			return
		}

		nowInLoc := time.Now().In(loc)
		utcOffset := nowInLoc.Format("UTC-07:00")
		isDST := nowInLoc.IsDST()
		start, end := nowInLoc.ZoneBounds()
		name, offset := nowInLoc.Zone()
		if strings.ContainsAny(name, "+-") {
			name = ""
		}
		renderTemplate(w, r, "timezone", map[string]any{
			"Timezone":       loc,
			"Abbr":           name,
			"UTCOffset":      utcOffset,
			"Offset":         offset,
			"IsDST":          isDST,
			"StartZoneBound": start,
			"EndZoneBound":   end,
			"NormalizedName": normalizedName,
		})
	})
	m.HandleFunc("/week", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache")

		_, weeknumber := time.Now().UTC().ISOWeek()
		renderTemplate(w, r, "week", map[string]any{
			"WeekNumber": weeknumber,
		})
	})
	m.HandleFunc("/iso3166", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache")
		renderTemplate(w, r, "iso3166", map[string]any{
			"ISO3166": tz.ISO3166List,
		})
	})
	m.HandleFunc("/map", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache")
		renderTemplate(w, r, "map", map[string]any{
			"MapTime": time.Now().UTC().Truncate(time.Minute).Unix(),
		})
	})

	m.HandleFunc("/map.png", func(w http.ResponseWriter, r *http.Request) {
		unixStr := r.URL.Query().Get("time")
		unix, err := strconv.ParseInt(unixStr, 10, 64)
		if err != nil {
			// Default to truncated to a minute current time.
			unix = time.Now().UTC().Truncate(time.Minute).Unix()
		}
		utcTime := time.Unix(unix, 0)

		zoomLevelStr := r.URL.Query().Get("zoom")
		zoomLevel, _ := strconv.Atoi(zoomLevelStr)
		zoomLevel = max(min(zoomLevel, 4), 1)

		refWorldMap := osm.Maps[zoomLevel]
		worldMap := image.NewRGBA(refWorldMap.Bounds())
		draw.Draw(worldMap, worldMap.Bounds(), refWorldMap, image.Point{}, draw.Over)

		osmsun.DrawSun(worldMap, utcTime, zoomLevel)
		osmsun.DrawMoon(worldMap, utcTime, zoomLevel)
		if err := osmsun.DrawAttribution(worldMap, zoomLevel, "Maps and Data © openstreetmap.org and contributors, ODbL"); err != nil {
			serverError(w, r, "DrawAttribution", err)
			return
		}

		bbytes := bytes.Buffer{}
		if err := png.Encode(&bbytes, worldMap); err != nil {
			serverError(w, r, "png.Encode", err)
			return
		}

		w.Header().Set("Cache-Control", "public, max-age=60, immutable")
		w.Header().Set("Content-Type", "image/png")

		// Not efficient conversion to bytes.Reader.
		http.ServeContent(w, r, "map.png", time.Now(), bytes.NewReader(bbytes.Bytes()))
	})

	m.HandleFunc("/diff", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache")
		if r.Method == http.MethodGet {
			renderTemplate(w, r, "diff", map[string]any{})
			return
		}

		getDate := func(prefix string) (time.Time, error) {
			sTimezone := r.FormValue(prefix + "-timezone")
			sLoc, _, _ := tz.LoadLocation(sTimezone)
			if sLoc == nil {
				sLoc = time.FixedZone("UTC", 0)
			}

			sToday := r.FormValue(prefix+"-date-today") == "on"
			var sYear int
			var sMonth time.Month
			var sDay int
			if sToday {
				sYear, sMonth, sDay = time.Now().In(sLoc).Date()
			} else {
				date, err := time.Parse("2006-01-02", r.FormValue(prefix+"-date"))
				if err != nil {
					return time.Now(), err
				}
				sYear, sMonth, sDay = date.Date()
			}

			sNow := r.FormValue(prefix+"-time-now") == "on"
			var sHour int
			var sMinute int
			var sSecond int
			if sNow {
				sHour, sMinute, sSecond = time.Now().In(sLoc).Clock()
			} else {
				timeVal := r.FormValue(prefix + "-time")
				if len(timeVal) != 0 {
					clock, err := time.Parse("15:04:05", timeVal)
					if err != nil {
						return time.Now(), err
					}
					sHour, sMinute, sSecond = clock.Clock()
				}
			}

			return time.Date(sYear, sMonth, sDay, sHour, sMinute, sSecond, 0, sLoc), nil
		}

		sDate, err := getDate("start")
		if err != nil {
			serverError(w, r, "getDate", err)
			return
		}
		eDate, err := getDate("end")
		if err != nil {
			serverError(w, r, "getDate", err)
			return
		}

		renderTemplate(w, r, "diff", map[string]any{
			"Form":       r.Form,
			"Difference": durafmt.Parse(eDate.Sub(sDate).Abs()).LimitFirstN(0),
		})
	})

	m.HandleFunc("/valid-timezone", func(w http.ResponseWriter, r *http.Request) {
		timeZone := r.URL.Query().Get("timezone")
		loc, _, err := tz.LoadLocation(timeZone)
		if err != nil {
			fmt.Fprint(w, `{"ok": false}`)
			return
		}
		fmt.Fprintf(w, `{"ok": true, "offset": %q}`, time.Now().In(loc).Format("UTC-07:00"))
	})

	m.HandleFunc("/sync", func(w http.ResponseWriter, _ *http.Request) {
		now := time.Now().UnixMilli()
		w.Header().Set("Cache-Control", "no-store")
		fmt.Fprint(w, strconv.FormatInt(now, 10))
	})
	m.HandleFunc("/assets/", handleAssets())

	return logHandler(m)
}

func serverError(w http.ResponseWriter, r *http.Request, funcName string, err error) {
	slog.Error("Internal Server Error", slog.Group("request", "method", r.Method, "url", r.URL), "error", err, "funcName", funcName)

	errText := http.StatusText(http.StatusInternalServerError)
	if logger.Enabled(context.Background(), slog.LevelDebug) {
		errText = err.Error()
	}
	http.Error(w, errText, http.StatusInternalServerError)
}

// Returns if the user agent _likely_ wants the content to be served in plain text.
func isPlainTextUserAgent(userAgent string) bool {
	return slices.ContainsFunc([]string{"curl", "Wget", "PowerShell"}, func(item string) bool { return strings.Contains(userAgent, item) })
}

func renderTemplate(w http.ResponseWriter, r *http.Request, tmpl string, data map[string]any) {
	if isPlainTextUserAgent(r.UserAgent()) && templates.Lookup(tmpl+".plain") != nil {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("X-Content-Type-Options", "nosniff")

		// Useful for plain templates to indicate if they only want the most crucial information.
		data["Short"], _ = strconv.ParseBool(r.URL.Query().Get("short"))
		if err := plainTemplates.ExecuteTemplate(w, tmpl+".plain", data); err != nil {
			serverError(w, r, "plainTemplates.ExecuteTemplate", err)
		}
		return
	}

	randomBytes := make([]byte, 16)
	if _, err := rand.Read(randomBytes); err != nil {
		serverError(w, r, "rand.Read", err)
		return
	}
	nonce := hex.EncodeToString(randomBytes)
	data["Nonce"] = nonce

	w.Header().Add("Content-Security-Policy", "base-uri 'self'; img-src 'self' data:; object-src 'none'; script-src 'nonce-"+nonce+"';")
	if err := templates.ExecuteTemplate(w, tmpl+".tmpl", data); err != nil {
		serverError(w, r, "templates.ExecuteTemplate", err)
	}
}

func handleAssets() func(http.ResponseWriter, *http.Request) {
	fileServer := http.FileServer(http.FS(assetsFS))
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "public, max-age=31536000, immutable")
		fileServer.ServeHTTP(w, r)
	}
}
