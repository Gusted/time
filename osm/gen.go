//go:build ignore

package main

import osmsun "codeberg.org/Gusted/osm-sun"

func main() {
	for i := range 4 {
		if _, err := osmsun.GetWorldMap("osm/", i+1); err != nil {
			panic(err)
		}
	}
}
