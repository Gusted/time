package osm

import (
	"embed"
	"image"
	"image/png"
	"strconv"
)

var (
	//go:embed *.png
	maps embed.FS

	Maps map[int]image.Image
)

func Initialize() error {
	Maps = map[int]image.Image{}

	dirEntries, err := maps.ReadDir(".")
	if err != nil {
		return err
	}

	for _, dirEntry := range dirEntries {
		f, err := maps.Open(dirEntry.Name())
		if err != nil {
			return err
		}
		defer f.Close()
		i, err := png.Decode(f)
		if err != nil {
			return err
		}
		zoomlevel, err := strconv.Atoi(dirEntry.Name()[4:5])
		if err != nil {
			return err
		}
		Maps[zoomlevel] = i
	}
	return nil
}
