package geonames

import (
	"bufio"
	"embed"
	"strconv"
	"strings"
)

type City struct {
	Name        string
	CountryCode string
	Timezone    string
	Population  int
}

type Capital struct {
	Name        string
	CountryName string
	CountryCode string
}

var (
	// Embed files of geonames.
	//
	//go:embed data/*.bin
	geonamesData embed.FS

	// A mapping of country name to their capital.
	CountryCapital = map[string]*Capital{}

	// A mapping of city name to a list of cities matching that name, which
	// contains information such as country and timezone.
	Cities = map[string][]*City{}
)

func init() {
	countryInfo, err := geonamesData.Open("data/countryInfo.bin")
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(countryInfo)
	for scanner.Scan() {
		line := scanner.Text()

		// The file is null byte delimited.
		columns := strings.Split(line, "\000")
		countryNameKey := columns[0]
		name := columns[1]
		countryName := columns[2]
		countryCode := columns[3]

		CountryCapital[countryNameKey] = &Capital{
			Name:        name,
			CountryName: countryName,
			CountryCode: countryCode,
		}
	}

	cities, err := geonamesData.Open("data/cities.bin")
	if err != nil {
		panic(err)
	}

	scanner = bufio.NewScanner(cities)
	for scanner.Scan() {
		// The file is null byte delimited.
		columns := strings.Split(scanner.Text(), "\000")

		city := columns[0]
		cityName := columns[1]
		country := columns[2]
		timezone := columns[3]
		population, _ := strconv.Atoi(columns[4])

		Cities[city] = append(Cities[city], &City{cityName, country, timezone, population})
		if len(columns) == 6 {
			asciiCity := columns[5]
			Cities[asciiCity] = append(Cities[asciiCity], &City{cityName, country, timezone, population})
		}
	}
}
