//go:build ignore

package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
)

func transformCountryInfo() error {
	countryInfoBin, err := os.OpenFile("geonames/data/countryInfo.bin", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0o600)
	if err != nil {
		return err
	}
	defer countryInfoBin.Close()
	countryInfoOutput := bufio.NewWriter(countryInfoBin)

	countryInfo, err := os.Open("geonames/data/countryInfo.txt")
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(countryInfo)
	for scanner.Scan() {
		line := scanner.Bytes()

		// Skip comments.
		if bytes.HasPrefix(line, []byte{'#'}) {
			continue
		}

		// The file is tab delimited.
		columns := bytes.Split(line, []byte{'\t'})
		countryCode := columns[0]
		countryName := columns[4]
		countryCapital := columns[5]

		// Skip if no capital is given.
		if len(countryCapital) == 0 {
			continue
		}

		// Normalize the country name, make it lowercase and strip 'the' prefix.
		countryNameKey := bytes.TrimPrefix(bytes.ToLower(countryName), []byte("the "))

		countryInfoOutput.Write(countryNameKey)
		countryInfoOutput.Write([]byte{0x00})
		countryInfoOutput.Write(bytes.ToLower(countryCapital))
		countryInfoOutput.Write([]byte{0x00})
		countryInfoOutput.Write(bytes.TrimPrefix(countryName, []byte("The ")))
		countryInfoOutput.Write([]byte{0x00})
		countryInfoOutput.Write(countryCode)
		countryInfoOutput.Write([]byte{'\n'})
	}

	return countryInfoOutput.Flush()
}

func transformCitiesInfo() error {
	citiesBin, err := os.OpenFile("geonames/data/cities.bin", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0o600)
	if err != nil {
		return err
	}
	defer citiesBin.Close()
	citiesOutput := bufio.NewWriter(citiesBin)
	cities15000, err := os.Open("geonames/data/cities15000.txt")
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(cities15000)
	for scanner.Scan() {
		// The file is tab delimited.
		columns := bytes.Split(scanner.Bytes(), []byte{'\t'})

		cityName := columns[1]
		city := bytes.ToLower(cityName)
		// ASCII representation of the city's name.
		asciiCity := bytes.ToLower(columns[2])
		country := columns[8]
		population := columns[14]
		timezone := columns[17]

		citiesOutput.Write(city)
		citiesOutput.Write([]byte{0x00})
		citiesOutput.Write(cityName)
		citiesOutput.Write([]byte{0x00})
		citiesOutput.Write(country)
		citiesOutput.Write([]byte{0x00})
		citiesOutput.Write(timezone)
		citiesOutput.Write([]byte{0x00})
		citiesOutput.Write(population)

		if !bytes.Equal(city, asciiCity) {
			citiesOutput.Write([]byte{0x00})
			citiesOutput.Write(asciiCity)
		}
		citiesOutput.Write([]byte{'\n'})
	}

	return citiesOutput.Flush()
}

func main() {
	fmt.Println("(i) Transform country info")
	if err := transformCountryInfo(); err != nil {
		fmt.Println("(!) could not pre-process the country info:", err)
		os.Exit(1)
	}

	fmt.Println("(i) Transform cities info")
	if err := transformCitiesInfo(); err != nil {
		fmt.Println("(!) could not transform the cities info:", err)
		os.Exit(1)
	}
}
