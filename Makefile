GO ?= go
TAGS ?=
ESBUILD_PACKAGE ?= github.com/evanw/esbuild/cmd/esbuild@latest
PICO_VERSION ?= v2.0.6
TZDATA_VERSION ?= 2025a

ifeq (, $(shell command -v $(GO)))
    $(error "Couldn't find $(GO) in $$PATH")
endif

.PHONY: all
all: build

.PHONY: build
build: build-frontend build-backend

.PHONY: test
test: tzdata geonames
	@go test ./...

.PHONY: build-frontend
build-frontend: download-pico
	@rm -rf assets/
	@GOARCH= GOOS= $(GO) run $(ESBUILD_PACKAGE) --minify --loader:.svg=dataurl --loader:.png=dataurl --banner:js='"use strict";' --bundle --platform=browser --outdir=assets web/{homepage.js,timezone.js,iso3166.js,week.js,diff.js,index.css}
	@cp web/favicon.svg assets/
	@cp -r web/images/. assets/

.PHONY: build-backend
build-backend: tzdata geonames osm
	@$(GO) build -tags $(TAGS) .

.PHONY: download-pico
download-pico:
ifeq (,$(wildcard .build-cache/$(PICO_VERSION).zip))
	@echo "Downloading Pico CSS $(PICO_VERSION)."
	@mkdir -p .build-cache
	@curl -L --no-progress-meter -o .build-cache/$(PICO_VERSION).zip https://github.com/picocss/pico/archive/refs/tags/$(PICO_VERSION).zip
	@unzip -p .build-cache/$(PICO_VERSION).zip pico-$(PICO_VERSION:v%=%)/css/pico.orange.css > web/pico.css
endif

.PHONY: tzdata
tzdata:
ifeq (,$(wildcard .build-cache/tzdata$(TZDATA_VERSION).tar.gz))
	@echo "Downloading tzdata $(TZDATA_VERSION)."
	@mkdir -p .build-cache
	@curl -L --no-progress-meter -o .build-cache/tzdata$(TZDATA_VERSION).tar.gz https://data.iana.org/time-zones/releases/tzdata$(TZDATA_VERSION).tar.gz
	@mkdir -p tz/tzdata
	@tar -xf .build-cache/tzdata$(TZDATA_VERSION).tar.gz -C tz/tzdata iso3166.tab
endif

.PHONY: geonames
geonames:
	@mkdir -p .build-cache geonames/data
	@curl --no-progress-meter --etag-compare .build-cache/countryInfo.etag --etag-save .build-cache/countryInfo.etag https://download.geonames.org/export/dump/countryInfo.txt -o geonames/data/countryInfo.txt
	@curl --no-progress-meter --etag-compare .build-cache/cities15000.etag --etag-save .build-cache/cities15000.etag https://download.geonames.org/export/dump/cities15000.zip -o .build-cache/cities15000.zip
	@unzip -q -d geonames/data -o .build-cache/cities15000.zip
	@GOARCH= GOOS= go run geonames/data_transform.go

.PHONY: osm
osm:
	@GOARCH= GOOS= go run osm/gen.go

.PHONY: clean
clean:
	@rm -rf assets/ tz/tzdata geonames/data .build-cache
