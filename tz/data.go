package tz

import (
	"bufio"
	"embed"
	"strings"
)

type Country struct {
	Name string
	Code string
}

var (
	// Embed some files of tzdata.
	//
	//go:embed tzdata/*
	tzData embed.FS

	// ISO3166List is an alphabetically sorted list of country name and ISO 3166
	// alpha-2 country code.
	ISO3166List = []*Country{}
)

func init() {
	iso3166, err := tzData.Open("tzdata/iso3166.tab")
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(iso3166)
	for scanner.Scan() {
		line := scanner.Text()

		// Skip comments.
		if strings.HasPrefix(line, "#") {
			continue
		}

		// Get the country code and name.
		countryCode, countryName, found := strings.Cut(line, "\t")
		if !found {
			continue
		}

		// Add the country code and name to the list, iso3166 is already
		// alphabetically sorted, so no need to do that here.
		ISO3166List = append(ISO3166List, &Country{
			Name: countryName,
			Code: countryCode,
		})
	}
}
