package tz

import (
	"testing"
	"time"
)

func TestLoadLocation(t *testing.T) {
	testCases := []struct {
		name           string
		normalizedName string
		offset         int
		valid          bool
	}{
		{"UTC", "UTC+0", 0, true},
		{"UTC0", "UTC+0", 0, true},
		{"UTC+0", "UTC+0", 0, true},
		{"UTC-0", "UTC+0", 0, true},
		{"UTC+14", "UTC+14", 14 * 3600, true},
		{"UTC-12", "UTC-12", -12 * 3600, true},
		{"UTC+15", "", 0, false},
		{"UTC-13", "", 0, false},
		{"EST", "EST", -5 * 3600, true},
		{"Europe/Amsterdam", "CET", 3600, true},
		{"Europe/London", "GMT", 0, true},
		{"Netherlands", "Netherlands", 3600, true}, // **The** Netherlands isn't a country.
		{"netherlands", "Netherlands", 3600, true}, // Lowercase.
		{"NETHERLANDS", "Netherlands", 3600, true}, // Uppercase.
		{"Amsterdam", "Amsterdam", 3600, true},
		{"Rotterdam", "Rotterdam", 3600, true},
		{"London", "London", 0, true},
		{"Genève", "Genève", 3600, true},                 // Official city name has UTF8.
		{"GENÈVE", "Genève", 3600, true},                 // Official city name has UTF8.
		{"Geneve", "Genève", 3600, true},                 // ASCII equilevant of the city name.
		{"United States", "United States", -18000, true}, // Should default to Washington D.C (has a lot of timezones).
		{"Washington", "Washington", -18000, true},       // There are a lot of cities named Washington.
		{"Paris", "Paris", 3600, true},                   // Texas has a city named Paris.
		{"Brazil", "Brazil", -10800, true},               // Should default to Brasília (ascii).
		{"Chad", "Chad", 3600, true},                     // Should default to N'Djamena (comma).
		{"Perth", "Perth", 28800, true},                  // Scotland has a city named Perth.
	}

	// Fix to a certain date to avoid DST messing with offsets.
	oneJan := time.Date(2024, time.January, 0, 0, 0, 0, 0, time.UTC)

	for _, testCase := range testCases {
		loc, name, err := LoadLocation(testCase.name)
		if testCase.valid {
			if err != nil {
				t.Fatalf("Location %q should be valid", testCase.name)
			}
			_, offset := oneJan.In(loc).Zone()
			if offset != testCase.offset {
				t.Errorf("Expected %d offset, got %d offset", testCase.offset, offset)
			}
			if name != testCase.normalizedName {
				t.Errorf("Expected normalized named %q, got %q", testCase.normalizedName, name)
			}
		} else if err == nil {
			t.Errorf("Location %q should be invalid", testCase.name)
		}
	}
}
