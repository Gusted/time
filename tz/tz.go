package tz

import (
	"cmp"
	"errors"
	"fmt"
	"slices"
	"strconv"
	"strings"
	"time"

	"codeberg.org/Gusted/time/geonames"
)

// Load location based on a IANA identifier or UTC offset. It returns
// time.Location that can be used by the "time" standard library and a
// normalize name to represent that location, this highly depends on the input,
// this not a canonical name to represent that location.
func LoadLocation(locationName string) (*time.Location, string, error) {
	loweredLocationName := strings.ToLower(locationName)

	// Check if it's an UTC
	if strings.HasPrefix(loweredLocationName, "utc") {
		// Handle UTC+0 case.
		if loweredLocationName == "utc" {
			return time.FixedZone("UTC", 0), "UTC+0", nil
		}

		offsetStr := strings.TrimPrefix(loweredLocationName, "utc")
		offset, err := strconv.Atoi(offsetStr)
		if err != nil {
			return nil, "", err
		}

		// Only allow valid UTC offsets. [-12,+14]
		if offset < -12 || offset > 14 {
			return nil, "", errors.New("invalid UTC offset")
		}

		// Normalized UTC string.
		normalizedUTCString := "UTC"
		if offset >= 0 {
			normalizedUTCString += "+"
		} else {
			normalizedUTCString += "-"
		}
		normalizedUTCString += strconv.Itoa(max(offset, -offset))

		return time.FixedZone("UTC"+offsetStr, offset*3600), normalizedUTCString, nil
	} else if capital, ok := geonames.CountryCapital[loweredLocationName]; ok {
		cities, ok := geonames.Cities[capital.Name]
		if !ok {
			return nil, "", fmt.Errorf("no city information found for capital %q", capital.Name)
		}
		// Since it is possible that there is more than one city with the same
		// name, we need to filter the results.
		b := cities[:0]
		for _, x := range cities {
			if x.CountryCode == capital.CountryCode {
				b = append(b, x)
			}
		}

		// Get the city with the highest number of population.
		city := slices.MaxFunc(b, func(a, b *geonames.City) int {
			return cmp.Compare(a.Population, b.Population)
		})

		loc, err := time.LoadLocation(city.Timezone)
		if err != nil {
			return nil, "", err
		}
		return loc, capital.CountryName, nil
	} else if cities, ok := geonames.Cities[loweredLocationName]; ok {
		// Get the city with the highest number of population.
		city := slices.MaxFunc(cities, func(a, b *geonames.City) int {
			return cmp.Compare(a.Population, b.Population)
		})

		loc, err := time.LoadLocation(city.Timezone)
		if err != nil {
			return nil, "", err
		}
		return loc, city.Name, nil
	}

	// Simply try to load the timezone.
	loc, err := time.LoadLocation(locationName)
	if err != nil {
		return nil, "", err
	}
	name, _ := time.Now().In(loc).Zone()
	return loc, name, nil
}
