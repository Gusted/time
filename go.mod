module codeberg.org/Gusted/time

go 1.23

require (
	codeberg.org/Gusted/osm-sun v0.0.0-20241222003926-964a022cee05
	github.com/hako/durafmt v0.0.0-20210608085754-5c1018a4e16b
	github.com/landlock-lsm/go-landlock v0.0.0-20250303204525-1544bccde3a3
)

require (
	golang.org/x/image v0.23.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/text v0.21.0 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.70 // indirect
)
