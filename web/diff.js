const debounce = (callback, wait) => {
  let timeoutId = null;

  return (...args) => {
    window.clearTimeout(timeoutId);

    timeoutId = window.setTimeout(() => {
      callback.apply(null, args);
    }, wait);
  };
}

(() => {
    for (const checkbox of document.querySelectorAll('#dates label:has(> :where(input[type="date"], input[type="time"])) ~ label > input[type="checkbox"]')) {
        checkbox.addEventListener('change', () => {
            const inputEl = checkbox.parentElement.previousElementSibling.querySelector('input');
            inputEl.disabled = checkbox.checked;
            if (inputEl.getAttribute('type') === 'date') {
                inputEl.toggleAttribute('required', !checkbox.checked);
            }
        });
    }
    for (const timezoneInput of document.querySelectorAll('#dates input[name$="timezone"]')) {
        timezoneInput.addEventListener('input', debounce(async () => {
            timezoneInput.previousElementSibling.setAttribute('aria-busy', 'true');
            const resp = await fetch(`/valid-timezone?timezone=${encodeURIComponent(timezoneInput.value)}`);
            const respJSON = await resp.json();
            const smallEl = timezoneInput.nextElementSibling;
            if (respJSON.ok) {
                timezoneInput.setAttribute('aria-invalid', 'false');
                smallEl.textContent = `Recognized timezone value with UTC offset: ${respJSON.offset}.`;
            } else {
                timezoneInput.setAttribute('aria-invalid', 'true');
                smallEl.textContent = 'The input is not a recognized timezone value.';
            }
            timezoneInput.previousElementSibling.setAttribute('aria-busy', 'false');
        }, 300));
    }
})(
)
