/**
 * Format the time
 * @param time {number}
 */
export const formatTime = (time) => {
    time = Math.abs(time);

    if (time < 1000) {
        return `${time} milliseconds`;
    }

    time /= 1000
    if (time < 60) {
        return `${time.toFixed(1)} seconds`;
    }

    time /= 60
    if (time < 60) {
        return `${time.toFixed(1)} minutes`;
    }

    time /= 60
    return `${time.toFixed(1)} hours`;
};

export let timeStatus = {
    // The absolute offset between the clocks of the user and server.
    offset: 0,
    // The delay that was added by the roundtrip.
    delay: 0,
};

/**
 * Use an simple NTP-based approach to determine the offset with the server.
 * Updates the `timeStatus` variable.
 */
export const updateTimeStatus = async () => {
    const clientTimestamp = Date.now();
    const response = await fetch(`/sync`, {cache: 'no-store'});
    const receivedTimestamp = Date.now();

    const responseBody = await response.text();
    const serverTimestamp = Number(responseBody);

    timeStatus.offset = ((serverTimestamp-clientTimestamp)+(serverTimestamp-receivedTimestamp))/2;
    timeStatus.delay = receivedTimestamp-clientTimestamp
};

let prevUnix;
const weekdayFormatter = new Intl.DateTimeFormat("en-US", {weekday: 'long'});
const monthFormatter = new Intl.DateTimeFormat("en-US", {month: 'long'});

export const replaceText = (el, text) => { if (el.textContent !== text) el.textContent = text };

/**
 * Update the clock with an given offset.
 * @param offset {number}
 */
export const updateClock = async (offset) => {
    const now = new Date(Date.now() + offset);
    const prev = prevUnix ?? now.getTime();
    prevUnix = now.getTime();

    replaceText(document.getElementById('clock-time'), [now.getHours(), now.getMinutes(), now.getSeconds()].map((timeUnit) => String(timeUnit).padStart(2, '0')).join(':'));
    replaceText(document.getElementById('calendar-date'), `${weekdayFormatter.format(now)}, ${String(now.getDate()).padStart(2, '0')} ${monthFormatter.format(now)} ${now.getFullYear()}`);

    document.title = `${[now.getHours(), now.getMinutes()].map((timeUnit) => String(timeUnit).padStart(2, '0')).join(':')} – ${(window.config?.timezone ?? "Time")}`;

    // If the previous invocation of this function differs more than a second,
    // it's likely that the machine's time has changed and that we should update
    // the time status. Value is 1250 to just be over a second, to account for
    // situations where the browser decides to clamp the minimum interval to be a second.
    if (Math.abs(prev - prevUnix) > 1250) updateTimeMessage();
};
