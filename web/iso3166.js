(() => {
    const tableHeaders = document.querySelectorAll('.sortable thead th');

    for (const th of tableHeaders) {
        const tbody = th.closest('table').querySelector('tbody');
        const thIdx = [...th.parentNode.children].indexOf(th);

        th.addEventListener('click', () => {
            // Get if current table header is set to descending.
            const descending = th.dataset.descending === 'true';
            // Reset the order on an all table theaders.
            for (const th of tableHeaders) th.dataset.descending = '';
            // Set the new order.
            th.dataset.descending = !descending;

            // Do the magical sorting.
            [...tbody.querySelectorAll('tr')]
                 .sort((a, b) => {
                     if (descending) [a, b] = [b, a];
                     return a.children[thIdx].textContent.localeCompare(b.children[thIdx].textContent);
                 })
                 .forEach(tr => tbody.append(tr));
        });
    }
})(
)
