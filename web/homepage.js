import {formatTime, timeStatus, updateTimeStatus, updateClock} from './utils.js';

const updateTimeMessage = async () => {
    const statusArticle = document.querySelector('#status article');
    statusArticle.style.height = `${statusArticle.getBoundingClientRect().height}px`;
    statusArticle.setAttribute('aria-busy', 'true');
    statusArticle.replaceChildren();

    await updateTimeStatus();
    const {offset, delay} = timeStatus;

    const statusMessage = document.createTextNode(`Your machine's clock is ${formatTime(offset)} ${offset > 0 ? 'behind' : 'ahead'} of the server's clock.`);
    const delayEl = document.createElement('small');
    delayEl.textContent = ` (Accuracy of ${formatTime(delay)})`;

    statusArticle.style.height = '';
    statusArticle.setAttribute('aria-busy', 'false');
    statusArticle.append(statusMessage);
    statusArticle.append(delayEl);
};

(async () => {
    await updateTimeMessage();
    setInterval(() => updateClock(timeStatus.offset), 50);
    setInterval(updateTimeMessage, 1000*60*5);
})();
