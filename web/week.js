import {timeStatus, replaceText, updateTimeStatus} from './utils.js';

const updateWeekNumber = () => {
    // Calculate the week number according to ISO 8601. Per their definition the
    // first week of a year contains the first Thursday of January of that year.
    // The calculation is derived from Go's ISOWeek function:
    // https://pkg.go.dev/time#Time.ISOWeek
    const now = new Date();
    // Unix epoch (in milliseconds).
    let abs = Date.now();
    // Calculate the offset from current day to Thursday in the calendar week.
    let d = 4 - now.getDay();
    // Handle Sunday.
    if (d === 4) d -= 7;
    // Find Thursday of the current calendar week.
    abs += d * 86400000
    // Calculate the day of the year of the Thursday.
    const dayOfYear = Math.floor((abs - new Date(now.getFullYear(), 0, 0)) / 86400000);
    const weekNumber = 1 + Math.floor(dayOfYear/7);
    replaceText(document.getElementById('week-number-value'), String(weekNumber));
};

(async () => {
    updateWeekNumber();
    setInterval(updateWeekNumber, 1000);
})();
