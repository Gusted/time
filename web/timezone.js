import {updateTimeStatus, timeStatus, updateClock, replaceText} from './utils.js';

const updateTimeZoneClock = () => {
    // The offset of the local timezone relative to UTC+0 in milliseconds.
    const timeZoneOffset = (new Date()).getTimezoneOffset()*-60000;

    // Calculate the offset that should be used. Start with the offset
    // between the server and the machine's clocks, then subtract the local
    // timezone to get to UTC+0, then add the given offset (in seconds) by
    // the server.
    const offset = timeStatus.offset - timeZoneOffset + window.config.offset*1000;
    updateClock(offset)
};

const setYourTimezone = () => {
    let timeZoneOffset = (new Date()).getTimezoneOffset();
    let UTCOffset = `UTC${timeZoneOffset > 0 ? '-' : '+'}`;
    timeZoneOffset = Math.abs(timeZoneOffset);
    UTCOffset += `${String(Math.floor(timeZoneOffset/60)).padStart(2, '0')}:${String(timeZoneOffset%60).padStart(2, '0')}`;
    document.querySelector("#your-timezone").dataset.tooltip = `Your timezone's offset is ${UTCOffset}`;
}

const calculateTimezoneDifference = () => {
    // The offset of the local timezone relative to UTC+0 in seconds.
    const timeZoneOffset = (new Date()).getTimezoneOffset()*-60;
    // The given offset by the server.
    const requestedTimeZoneOffset = window.config.offset;

    // Calculate the difference in seconds between the two offsets.
    let diffOffset = Math.abs(timeZoneOffset-requestedTimeZoneOffset);
    const diffOffsetEl = document.querySelector("#diff-timezone");

    // Simple case.
    if (diffOffset === 0) {
        diffOffsetEl.textContent = 'none.';
        return;
    }

    const timeUnits = [];
    if (diffOffset >= 3600) {
        timeUnits.push({value: diffOffset/3600, unit: 'hour'});
        diffOffset %= 3600;
    }
    if (diffOffset >= 60) {
        timeUnits.push({value: diffOffset/60, unit: 'minute'});
        diffOffset %= 60;
    }
    if (diffOffset > 0) {
        timeUnits.push({value: diffOffset, unit: 'second'});
    }
    replaceText(diffOffsetEl, `${timeUnits.map((el) => `${Math.floor(el.value)} ${el.unit}${el.value >= 2 ? 's' : ''}`).join(' and ')}.`)
};

(async () => {
    await updateTimeStatus();
    calculateTimezoneDifference();
    setYourTimezone();
    setInterval(updateTimeStatus, 1000*60*5);
    setInterval(updateTimeZoneClock, 50);
})();
