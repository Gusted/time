package main

import (
	"context"
	"flag"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"time"

	"codeberg.org/Gusted/time/osm"
	"github.com/landlock-lsm/go-landlock/landlock"
)

var (
	debug = flag.Bool("debug", false, "Enable debug messages")
	port  = flag.Int("port", 8080, "Specify the port where the server listens to")
	host  = flag.String("host", "127.0.0.1", "Specify the host where the server listens on")
)

var logger *slog.Logger

func initLogger() {
	logLevel := slog.LevelInfo
	if *debug {
		logLevel = slog.LevelDebug
	}

	logger = slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: logLevel}))
}

// Listen for the interrupt signal and upon receiving the signal, shutdown the
// HTTP server and signal that the program can be exited.
func listenSignals(srv *http.Server, done chan struct{}) {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint

	logger.Info("Shutdown signal received")

	// We received an interrupt signal, shut down.
	if err := srv.Shutdown(context.Background()); err != nil {
		logger.Error("HTTP server Shutdown", "error", err)
	}
	close(done)
}

func initLandlock() {
	if err := landlock.V5.BestEffort().Restrict(
		// Where the server will listen on
		landlock.BindTCP(uint16(*port)),

		// Timezone stuff that Go's `time` package will query.
		// https://cs.opensource.google/go/go/+/refs/tags/go1.24.1:src/time/zoneinfo_unix.go;l=18-26
		landlock.RODirs("/usr/share/zoneinfo/").IgnoreIfMissing(),
		landlock.RODirs("/usr/share/lib/zoneinfo/").IgnoreIfMissing(),
		landlock.RODirs("/usr/lib/locale/TZ/").IgnoreIfMissing(),
		landlock.ROFiles("/etc/zoneinfo").IgnoreIfMissing(),
		// https://cs.opensource.google/go/go/+/refs/tags/go1.24.1:src/time/zoneinfo_unix.go;l=39
		landlock.ROFiles("/etc/localtime").IgnoreIfMissing(),
		// https://cs.opensource.google/go/go/+/refs/tags/go1.24.1:src/time/zoneinfo_goroot.go;l=13
		landlock.ROFiles(runtime.GOROOT()+"/lib/time/zoneinfo.zip").IgnoreIfMissing(),

		// Linux net stuff
		// https://cs.opensource.google/go/go/+/refs/tags/go1.24.1:src/net/sock_linux.go;l=35
		landlock.ROFiles("/proc/sys/net/core/somaxconn"),

		// Mime determing
		// https://cs.opensource.google/go/go/+/refs/tags/go1.24.1:src/mime/type_unix.go;l=26-32
		landlock.ROFiles("/etc/mime.types").IgnoreIfMissing(),
		landlock.ROFiles("/etc/apache2/mime.types").IgnoreIfMissing(),
		landlock.ROFiles("/etc/apache/mime.types").IgnoreIfMissing(),
		landlock.ROFiles("/etc/httpd/conf/mime.types").IgnoreIfMissing(),
		// https://cs.opensource.google/go/go/+/refs/tags/go1.24.1:src/mime/type_unix.go;l=19-24
		landlock.ROFiles("/usr/local/share/mime/globs2").IgnoreIfMissing(),
		landlock.ROFiles("/usr/share/mime/globs2").IgnoreIfMissing(),
	); err != nil {
		logger.Error("Could not enable landlock", "error", err)
		os.Exit(1)
	}
	logger.Info("Successfully enabled landlock")
}

func main() {
	flag.Parse()
	initLogger()
	initLandlock()
	if err := osm.Initialize(); err != nil {
		logger.Error("Could not initialize osm maps", "error", err)
		os.Exit(1)
	}

	addr := net.JoinHostPort(*host, strconv.Itoa(*port))
	s := &http.Server{
		Addr:           addr,
		Handler:        routes(),
		ReadTimeout:    30 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	done := make(chan struct{})
	go listenSignals(s, done)

	fmt.Printf("Started listing on http://%s\n", addr)
	if err := s.ListenAndServe(); err != http.ErrServerClosed {
		logger.Error("HTTP server ListenAndServe", "error", err)
		os.Exit(1)
	}

	<-done
}
