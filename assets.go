package main

import (
	"crypto/sha256"
	"embed"
	"encoding/hex"
	"fmt"
	htmlTemplate "html/template"
	"io"
	stdDebug "runtime/debug"
	"slices"
	"strings"
	"text/template"
	"time"
	"unicode/utf8"

	"codeberg.org/Gusted/time/tz"
)

func init() {
	buildInfo, ok := stdDebug.ReadBuildInfo()
	if !ok {
		return
	}

	vcsRevisionIdx := slices.IndexFunc(buildInfo.Settings, func(item stdDebug.BuildSetting) bool {
		return item.Key == "vcs.revision"
	})
	if vcsRevisionIdx == -1 {
		return
	}

	vcsRevision = buildInfo.Settings[vcsRevisionIdx].Value
}

var (
	// Stores the version control system revision that the program was built with.
	vcsRevision = "development"
	// Embed all assets that's used by the program into the binary.
	//
	//go:embed assets/* templates/*
	assetsFS embed.FS

	// Store SHA256 hashes of files in assetsFS.
	assetHashes = map[string]string{}

	// Stores parsed templates, along with functions that can be called from
	// the templates.
	templates = htmlTemplate.Must(htmlTemplate.New("").Funcs(htmlTemplate.FuncMap{
		"Asset": func(fileName string) string {
			if hash, ok := assetHashes[fileName]; ok {
				return fmt.Sprintf("/assets/%s?v=%s", fileName, hash)
			}

			f, err := assetsFS.Open("assets/" + fileName)
			if err != nil {
				panic(err)
			}

			h := sha256.New()
			_, _ = io.Copy(h, f)

			hash := hex.EncodeToString(h.Sum(nil))
			assetHashes[fileName] = hash

			return fmt.Sprintf("/assets/%s?v=%s", fileName, hash)
		},
		"FormatTime": formatTime,
		"BuildInfo": func() string {
			return vcsRevision
		},
	}).ParseFS(assetsFS, "templates/*.tmpl"))

	// Stores parsed plain templates.
	plainTemplates = template.Must(template.New("").Funcs(template.FuncMap{
		"LongestCountryName": func(x []*tz.Country) int {
			longestLen := 0
			for _, item := range x {
				longestLen = max(longestLen, len(item.Name))
			}
			return longestLen
		},
		"StringRepeat": strings.Repeat,
		"RuneCount":    utf8.RuneCountInString,
		"FormatTime":   formatTime,
		"add": func(a, b int) int {
			return a + b
		},
		"sub": func(a, b int) int {
			return a - b
		},
	}).ParseFS(assetsFS, "templates/*.plain"))
)

func formatTime(format string, loc ...*time.Location) string {
	if len(loc) == 1 {
		return time.Now().In(loc[0]).Format(format)
	}
	return time.Now().UTC().Format(format)
}
